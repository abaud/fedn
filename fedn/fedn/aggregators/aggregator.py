from abc import ABC, abstractmethod
from fedn.aggregators.custom import custom_aggregators

class AggregatorBase(ABC):
    """ Abstract class defining helpers. """

    @abstractmethod
    def __init__(self, id, storage, server, modelservice, control):
        """ """
        self.name = ""
        self.storage = storage
        self.id = id
        self.server = server
        self.modelservice = modelservice
        self.control = control

    @abstractmethod
    def on_model_update(self, model_id):
        pass

    @abstractmethod
    def on_model_validation(self, validation):
        pass

    @abstractmethod
    def combine_models(self, nr_expected_models=None, nr_required_models=1, helper=None, timeout=180):
        pass
   
  
def get_aggregator(aggregator_type, id, storage, server, modelservice, control):
    """ Return an instance of the aggregator class. 

    :param aggregator_type (str): The aggregator type ('fedavg')
    :return: 
    """
    if aggregator_type == 'fedavg':
        from fedn.aggregators.fedavg import FedAvgAggregator
        return FedAvgAggregator(id, storage, server, modelservice, control)
    
    func = custom_aggregators.get(aggregator_type)
    if func is not None : 
        from fedn.aggregators.fedcustom import FedCustomAggregator
        return FedCustomAggregator(id, storage, server, modelservice, control, aggregator_type, func)
    
    print("AGGREGATOR: COULD NOT FIND AGGREGATOR {}, defaulting to fedavg".format(aggregator_type))
    from fedn.aggregators.fedavg import FedAvgAggregator
    return FedAvgAggregator(id, storage, server, modelservice, control)

def list_aggregator_type():
    agg_list = [*custom_aggregators.keys()]
    agg_list.append('fedavg')
    return agg_list
        



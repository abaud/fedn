# TP Fedn
## Install Fedn
setup de Fedn et du projet MNIST
- cloner le repository `git clone https://gitlab.inria.fr/abaud/fedn`
- passer sur la branche TP : `git checkout TP`
- `cd fedn/examples/mnist-keras`
- mise en place d'un environnement python avec ses requirements : `bin/init_venv.sh` 
- téléchargement des données de MNIST : `bin/get_data`, elles seront téléchargées dans le dossier `fedn/examples/mnist-keras/data`
- split des données pour plusieurs clients : `bin/split_data --n_splits=4` on utilisera 4 clients pendant ce TP. Les données de chaque client se trouvent dans `data/clients`
- création d'un modèle initial (seed) : `bin/build.sh`

Test de l'installation : 
- ` docker-compose -f ..\..\docker-compose.yaml -f .\docker-compose.override.yaml up --scale client=4`. utilise le docker-compose file du projet fedn, avec les modifications spécifiques de `docker-compose.override.yaml`, puis spécifie le nombre de services client à utiliser avec `--scale client=4` 
- dans un navigateur : https://localhost:8090 (attention aucun certificat généré donc le navigateur vous dissuadera d'accéder au site).
- upload de la seed initial (`fedn/examples/mnist-keras/seed.npz` généré durant la 6ᵉ étape d'installation)
- Dans control, start run avec 5 rounds.
- regardez l'évolution des résultats dans l'onglet Dashboard
- stoppez fedn : `docker-compose -f ..\..\docker-compose.yaml -f .\docker-compose.override.yaml down`

Prise en main du script client se trouvant dans `client/entrypoint`
## 1ʳᵉ Attaque (client adversaire)
Dans cette attaque, un client adversaire tente de casser la convergence du modèle fédéré. Pour cela, les poids qu'il remonte au serveur d'agrégation sont aléatoires.
Dans `client-adversarial/entrypoint`, modifiez la fonction `train()` pour qu'elle renvoie un ensemble de poids aléatoire.

Une fois fait, testez votre solution en lançant le docker-compose avec un client adversaire :
`docker-compose -f ..\..\docker-compose.yaml -f .\docker-compose.override.yaml up --scale client=3 --scale client-adversarial=1`

## Défense (FedMedian)
Une défense face à un ou plusieurs clients adversaires de ce type est d'utiliser une autre forme d'agrégation moins sensible aux valeurs extrêmes, par exemple utiliser une médiane au lieu d'une moyenne.
Dans `fedn/fedn/aggregators/custom/__init__.py`, en prenant exemple sur `custom_avg()` qui fait une moyenne, écrivez une fonction d'agrégation qui retourne la médiane des poids reçus. Rajoutez ensuite cette fonction dans `custom_aggregators` comme pour l'exemple de "customavg"
Testez votre solution (vous devrez re-build votre docker-compose : `docker-compose -f ..\..\docker-compose.yaml -f .\docker-compose.override.yaml up --scale client=3 --scale client-adversarial=1 --build`) en changeant l'aggregator utilisé dans l'onglet Control avant de démarrer la run.
Vous pouvez aussi changer le nombre de clients adversaire pour vérifier l'impact de cette solution face à plus de client adversaire.

## Membership attack
une attaque de Membership vise à savoir si les données d'un client faisaient partie du jeu de données lors de l'entrainement d'un modèle.
Pour cela, nous pouvons regarder la loss du modèle quand il évalue un client. Celle-ci sera plus haute pour un client que le modèle ne connait pas, ainsi avec un seuil bien choisi, nous pouvons différencier des clients faisant partie du jeu d'entrainement, d'un client nouveau.
En utilisant le modèle situé à `examples/motion/client/` et les poids pré-entrainé `examples/motion/seed_50_avg.npz`, retrouvez le ou lesquels des clients (que vous trouverez  dans `examples/motion/data/clients/`) n'étaient pas présent lors de l'entrainement.
Utilisez le code de `examples/motion/client/validate.py` comme point de départ.


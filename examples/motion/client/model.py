#!./.fl-act/bin/python
import tensorflow as tf
import numpy as np
from fedn.utils.kerashelper import KerasHelper
import fire
import json
import docker
import os

NUM_CLASSES=10

#hyperparameters
E = 3
B = 256

NUM_ACT_LABELS = 4
AVAIL_CLIENTS = list(range(1,25))

## removing some clients
###

def _get_data_path():
    # Figure out FEDn client number from container name
    client = docker.from_env()
    container = client.containers.get(os.environ['HOSTNAME'])
    container_id = int(container.name.split("-")[-1]) - 1
    client_id = AVAIL_CLIENTS[container_id]
    
    # Return data path
    return f"/var/data/clients/{client_id}/motion.npz"


def _motion_model(p_width=50, p_height=12):
    '''
    Returns a convolutional model.
    
            Parameters:
                    none
    
            Returns:
                    model (model): a convolutional model.
    '''
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(p_height, p_width,1)))
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Dense(1000, activation='relu', kernel_initializer='he_uniform'))#1000
    model.add(tf.keras.layers.Dense(500, activation='relu', kernel_initializer='he_uniform'))#500
    model.add(tf.keras.layers.Dense(500, activation='relu', kernel_initializer='he_uniform'))#100
    model.add(tf.keras.layers.Flatten())
    
    model.add(tf.keras.layers.Dense(NUM_ACT_LABELS, activation='softmax'))
    # compile model
    
    model.compile(optimizer="Adam", loss='categorical_crossentropy', metrics=['accuracy'])
    return model


def _load_data(data_path, is_train=True):
    # Load data
    if data_path is None:
        data = np.load(_get_data_path())
    else:
        data = np.load(data_path)

    if is_train:
        X = data['x_train']
        y = data['y_train']
    else:
        X = data['x_test']
        y = data['y_test']

    # Normalize
    X = X.astype('float32')
    #X = np.expand_dims(X,-1)
    #X = X / 255
    #y = tf.keras.utils.to_categorical(y, NUM_CLASSES)

    return X, y



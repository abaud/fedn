from model import _motion_model, _load_data
from fedn.utils.kerashelper import KerasHelper
import fire
import os



def train(in_model_path, out_model_path, data_path=None, batch_size=32, epochs=1):
    # Load data
    x_train, y_train = _load_data(data_path)

    # Load model
    model = _motion_model()
    helper = KerasHelper()
    weights = helper.load_model(in_model_path)
    model.set_weights(weights)

    # Train
    model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs)
    
    # Save
    weights = model.get_weights()
    helper.save_model(weights, out_model_path)

if __name__ == "__main__" :
    fire.Fire(train)

from model import _motion_model
from fedn.utils.kerashelper import KerasHelper
import fire

def init_seed(out_path='seed.npz'):
	weights = _motion_model().get_weights()
	helper = KerasHelper()
	helper.save_model(weights, out_path)

if __name__ == "__main__" :
    fire.Fire(init_seed)

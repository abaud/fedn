from model import _load_data, _motion_model
import json
import numpy as np
from fedn.utils.kerashelper import KerasHelper
import fire


def validate(in_model_path, out_json_path=None, data_path=None, verbose="auto"):
    # Load data
    x_train, y_train = _load_data(data_path)
    x_test, y_test = _load_data(data_path, is_train=False)

    # Load model
    model = _motion_model()
    helper = KerasHelper()
    weights = helper.load_model(in_model_path)
    model.set_weights(weights)

    # Evaluate
    model_score = model.evaluate(x_train, y_train, verbose=0)
    model_score_test = model.evaluate(x_test, y_test, verbose=0)
    y_pred = model.predict(x_test)
    y_pred = np.argmax(y_pred, axis=1)

    # JSON schema
    report = {
        "training_loss": model_score[0],
        "training_accuracy": model_score[1],
        "test_loss": model_score_test[0],
        "test_accuracy": model_score_test[1],
    }

    if out_json_path != None :
        # Save JSON
        with open(out_json_path,"w") as fh:
            fh.write(json.dumps(report))
    else : 
        print(json.dumps(report))

if __name__ == "__main__" :
    fire.Fire(validate)

#!/bin/bash
set -e

# Init venv
python -m venv .fl-act

# Pip deps
.fl-act/bin/pip install --upgrade pip
.fl-act/bin/pip install -e ../../fedn
.fl-act/bin/pip install -r requirements.txt

import numpy as np

def custom_avg(models):
    out = []
    for i in range(len(models[0])):
        out.append(np.sum([x[i] for x in models], axis=0))
        out[i] /= len(models)
    return out

def random_fed(models):
    chosen = np.random.randint(0, len(models))
    return models[chosen]

custom_aggregators = { "custom_avg" : custom_avg, "random_fed" : random_fed }
